<?php

/**
 * Panels mini plugin for Config To Code (c2c) module.
 * @file c2c_panels.inc
 */

/**
 * Implementation of hook_c2c_build_export().
 */
function panels_mini_c2c_build_export(&$file, $panel) {
  $file['contents'] = panels_mini_export($panel);
  $file['return_variable'] = 'mini';
}

/**
 * Implementation of hook_c2c_fetch_items().
 */
function panels_mini_c2c_fetch_items($names = NULL) {
  // This is based on panels_mini_load_all, but does not try and incorporate default panels
  $panels = $dids = array();
  $query = "SELECT * FROM {panels_mini}";

  if ($names) {
    $query .= " WHERE name IN (" . implode(', ', array_fill(0, sizeof($names), "'%s'")) . ")";
  }

  $result = db_query($query, $names);


  while ($panel_mini = db_fetch_object($result)) {
    $panel_mini->contexts = (!empty($panel_mini->contexts)) ? unserialize($panel_mini->contexts) : array();
    $panel_mini->requiredcontexts = (!empty($panel_mini->requiredcontexts)) ? unserialize($panel_mini->requiredcontexts) : array();
    $panel_mini->relationships = (!empty($panel_mini->relationships)) ? unserialize($panel_mini->relationships) : array();
    $panel_mini->category = (!empty($panel_mini->category)) ? $panel_mini->category : 'Mini panels';
    $panel_mini->hide_title = ((bool) db_result(db_query('SELECT hide_title FROM {panels_display} WHERE did = %d', $panel_mini->did)));

    $panel_mini->type = t('Local');
    $panels[$panel_mini->name] = panels_mini_sanitize($panel_mini);
  }
  
  return array(
    'panels_mini' => array(
      'name' => 'Panel Minis',
      'items' => $panels,
    ),
  );
}

/**
 * Implementation of hook_c2c_delete_config().
 */
function panels_mini_c2c_delete_config($name) {
  panels_mini_delete(panels_mini_load($name));
}

/**
 * Implementation of hook_default_panel_minis().
 * 
 * @return array of panels minis keyed by name.
 */
function c2c_default_panel_minis() {
  $panels = array();
  $file_names = c2c_files('panels_mini');
  foreach ($file_names as $file_name) {
    require_once($file_name->filename);
    $func = c2c_name_to_function_name('panels_mini', $file_name->name);
    if (function_exists($func)) {
      $panels[$file_name->name] = $func();
    }
  }
  return $panels;
}
