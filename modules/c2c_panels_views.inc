<?php

/**
 * Panels views plugin for Config To Code (c2c) module.
 * @file c2c_panels.inc
 */

/**
 * Implementation of hook_c2c_build_export().
 */
function panels_views_c2c_build_export(&$file, $panel) {
  $file['contents'] = panels_views_export($panel);
  $file['return_variable'] = 'panel_view';
}

/**
 * Implementation of hook_c2c_fetch_items().
 */
function panels_views_c2c_fetch_items($names = NULL) {
  // This is based on panels_views_load_all, but does not try and incorporate default panels
  $pages = $dids = array();
  $query = "SELECT * FROM {panels_views}";

  if ($names) {
    $query .= " WHERE name IN (" . implode(', ', array_fill(0, sizeof($names), "'%s'")) . ")";
  }

  $result = db_query($query, $names);

  $panel_views = array();
  $fields = panels_views_pane_fields();

  while ($pv = db_fetch_object($result)) {
    foreach ($fields as $name => $data) {
      if (!empty($data['serialize'])) {
        $pv->$name = unserialize($pv->$name);
      }
    }
    $pv->type = t('Local');
    $panel_views[$pv->name] = $pv;
  }

  return array(
    'panels_views' => array(
      'name' => 'Panel Views',
      'items' => $panel_views,
    ),
  );
}

/**
 * Implementation of hook_c2c_delete_config().
 */
function panels_views_c2c_delete_config($name) {
  panels_views_delete(panels_views_load($name));
}

/**
 * Implementation of hook_default_panel_views().
 * 
 * @return array of panels pages keyed by name.
 */
function c2c_default_panel_views() {
  $panels = array();
  $file_names = c2c_files('panels_views');
  foreach ($file_names as $file_name) {
    require_once($file_name->filename);
    $func = c2c_name_to_function_name('panels_views', $file_name->name);
    if (function_exists($func)) {
      $panels[$file_name->name] = $func();
    }
  }
  return $panels;
}
