<?php

/**
 * Views plugin for Config To Code (c2c) module.
 * @file c2c_views.inc
 */

/**
 * Implementation of hook_c2c_build_export().
 */
function views_c2c_build_export(&$file, $vid) {
  $file['contents'] = views_create_view_code($vid);
  $file['return_variable'] = 'view';
}

/**
 * Implementation of hook_c2c_fetch_items().
 */
function views_c2c_fetch_items($names = NULL) {
  $query = 'SELECT vid, name FROM {view_view}';
  if ($names) {
    $query .= " WHERE name IN (" . implode(', ', array_fill(0, sizeof($names), "'%s'")) . ")";
  }
  $result = db_query($query, $names);
  
  $views = array();
  while ($view = db_fetch_object($result)) {
    $views[$view->name] = $view->vid;
  }
  
  return array(
    'views' => array(
      'name' => 'Views',
      'items' => $views,
    ),
  );
}

/**
 * Implementation of hook_c2c_delete_config().
 */
function views_c2c_delete_config($vid) {
  _views_delete_view(views_get_view($vid));
  views_invalidate_cache();
}

/**
 * Implementation of hook_views_default_views().
 * 
 * @return array of views objects keyed by name.
 */
function c2c_views_default_views() {
  $views = array();
  $file_names = c2c_files('views');
  foreach ($file_names as $file_name) {
    require_once($file_name->filename);
    $func = c2c_name_to_function_name('views', $file_name->name);
    if (function_exists($func)) {
      $views[$file_name->name] = $func();
    }
  }
  return $views;
}
