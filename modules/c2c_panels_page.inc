<?php

/**
 * Panels page plugin for Config To Code (c2c) module.
 * @file c2c_panels.inc
 */

/**
 * Implementation of hook_c2c_build_export().
 */
function panels_page_c2c_build_export(&$file, $panel) {
  $file['contents'] = panels_page_export($panel);
  $file['return_variable'] = 'page';
}

/**
 * Implementation of hook_c2c_fetch_items().
 */
function panels_page_c2c_fetch_items($names = NULL) {
  // This is based on panels_page_load_all, but does not try and incorporate default panels
  $pages = $dids = array();
  $query = "SELECT * FROM {panels_page}";

  if ($names) {
    $query .= " WHERE name IN (" . implode(', ', array_fill(0, sizeof($names), "'%s'")) . ")";
  }

  $result = db_query($query, $names);

  while ($page = db_fetch_object($result)) {
    $page->access = ($page->access ? explode(', ', $page->access) : array());
    $page->arguments = (!empty($page->arguments)) ? unserialize($page->arguments) : array();
    $page->displays = (!empty($page->displays)) ? unserialize($page->displays) : array();
    $page->contexts = (!empty($page->contexts)) ? unserialize($page->contexts) : array();
    $page->relationships = (!empty($page->relationships)) ? unserialize($page->relationships) : array();
    $page->switcher_options = (!empty($page->switcher_options)) ? unserialize($page->switcher_options) : array();

    $page->type = t('Local');
    $pages[$page->name] = panels_page_sanitize($page);
  }
  
  return array(
    'panels_page' => array(
      'name' => 'Panel Pages',
      'items' => $pages,
    ),
  );
}

/**
 * Implementation of hook_c2c_delete_config().
 */
function panels_page_c2c_delete_config($name) {
  panels_page_delete(panels_page_load($name));
}

/**
 * Implementation of hook_default_panel_pages().
 * 
 * @return array of panels pages keyed by name.
 */
function c2c_default_panel_pages() {
  $panels = array();
  $file_names = c2c_files('panels_page');
  foreach ($file_names as $file_name) {
    require_once($file_name->filename);
    $func = c2c_name_to_function_name('panels_page', $file_name->name);
    if (function_exists($func)) {
      $panels[$file_name->name] = $func();
    }
  }
  return $panels;
}
